import { Component, OnInit } from '@angular/core';
import { Product, ProductsService } from '../services/products.service';

@Component({
  selector: 'app-productsearch',
  templateUrl: './productsearch.page.html',
  styleUrls: ['./productsearch.page.scss'],
})
export class ProductSearchPage implements OnInit {

  products: Product[];

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.productsService.getProducts().subscribe(res => {
      this.products = res;
    });
  }

}
