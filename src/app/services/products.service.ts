import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

export interface Product {
  id?: string;
  title: string;
  description: string;
  order: number;
}

@Injectable({
  providedIn: 'root'
})

export class ProductsService {
  private productsCollection: AngularFirestoreCollection<Product>;
  private products: Observable<Product[]>;

  constructor(db: AngularFirestore) {
    this.productsCollection = db.collection<Product>('products');

    this.products = this.productsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data};
        });
      })
    );
  }

  getProducts(): Observable<Product[]> {
    return this.products;
  }

  getProduct(id: string) {
    return this.productsCollection.doc<Product>(id).valueChanges();
  }
}

